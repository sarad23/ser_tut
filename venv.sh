#!/usr/bin/env bash
DIRECTORY=virtualenvs/.test1
deactivate 2> /dev/null
if [ -d "${DIRECTORY}" ]; then
    source ${DIRECTORY}/bin/activate
else
    virtualenv -p `which python3` ${DIRECTORY}
    source ${DIRECTORY}/bin/activate
#pip install -r requirements.txt
fi