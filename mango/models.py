# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings

# Create your models here.

class Employee(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='user_roles')
    ended_at = models.DateTimeField(blank=True, null=True)

class Book(models.Model):
    name = models.CharField(max_length=200, verbose_name='First Name in English')
    user = models.ForeignKey('Employee', related_name='user_designation')

class Song(models.Model):
    name = models.CharField(max_length=200)
    album = models.ForeignKey('Album')

class Album(models.Model):
    name = models.CharField(max_length=200)


class School(models.Model):
    name = models.CharField(max_length=200)

class Teacher(models.Model):
    name = models.CharField(max_length=200)
    school = models.ForeignKey('School')


class Person(models.Model):
    name = models.CharField(max_length=200)
    age = models.CharField(max_length=200, null=True)
    city = models.ForeignKey('City')
    house = models.ForeignKey('House')

class City(models.Model):
    name = models.CharField(max_length=200)

class House(models.Model):
    code = models.CharField(max_length=200)

class Logs(models.Model):
    name = models.CharField(max_length=200)
    person = models.ForeignKey('Person', related_name="person_logs")

class Apple(models.Model):
    name = models.CharField(max_length=200)

    def model_method(self):
        return "some_calculated_result"