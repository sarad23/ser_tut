from mango.models import *
from rest_framework import serializers

class TrackListingField(serializers.RelatedField):
    def to_representation(self, value):
        print('**********')
        print(value)
        return {'name': value.name}

    def to_internal_value(self, data):
        print("&&&&&&&&&&&&")
        print(data)
        return Album.objects.get(id=data)

class SongS(serializers.ModelSerializer):
    album = TrackListingField(queryset=Album.objects.all())
    class Meta:
        model = Song
        fields = ('__all__')


class SS(serializers.ModelSerializer):
    class Meta:
        model = School
        fields = ('__all__')


class TS(serializers.ModelSerializer):

    school = serializers.SerializerMethodField('get_schools')
    def get_schools(self, obj):
        s = School.objects.filter(id=obj.school.id)
        ss = SS(instance=s)
        return ss
    class Meta:
        model = Teacher
        fields = ('name','school')

class CField(serializers.RelatedField):
    def to_representation(self, value):
        print('**********')
        print(value)
        return {'name': value.name}

    def to_internal_value(self, data):
        print("&&&&&&&&&&&&")
        print(data)
        return City.objects.get(id=data)

class HS(serializers.ModelSerializer):
    class Meta:
        model = House
        fields = ('__all__')

class LS(serializers.ModelSerializer):
    class Meta:
        model = Logs
        fields = ('name',)

class PS(serializers.ModelSerializer):
    house = HS()
    logs = LS(many=True)
    city = CField(queryset=City.objects.all())
    class Meta:
        model = Person
        fields = ('house','age','logs', 'name', 'city')

    def create(self, v_data):
        print('here')
        print('********')
        print(v_data['name'])
        house_data = v_data.pop('house')
        logs_data = v_data.pop('logs')
        house = House.objects.create(**house_data)
        person = Person.objects.create(house=house,**v_data)
        for log_data in logs_data:
            Logs.objects.create(person=person, **log_data)
        return person

    def update(self, ins, v_data):
        pass


class AS(serializers.ModelSerializer):
    url = serializers.CharField(source='model_method', read_only=True)
    ball = serializers.CharField(max_length=200)

    class Meta:
        model = Apple
        fields = ('name', 'ball', 'url')

    def create(self, v_data):
        ball = v_data.pop('ball')
        # do something
        name = v_data.pop('name')
        apple = Apple.objects.create(name=name)
        return apple