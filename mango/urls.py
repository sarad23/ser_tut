from django.conf.urls import url
from .views import employee, test

from django.contrib.auth.decorators import login_required

urlpatterns = [
        url(r'^e/$', employee, name='employee'),
        url(r'^f/$', test, name='test')
]