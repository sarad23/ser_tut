# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-14 03:45
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mango', '0007_auto_20171113_1651'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='age',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
