from mango.models import Employee, Book
from django.contrib.auth.models import User
from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username','password', 'email')

class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = ('user', 'ended_at')

class TrackListingField(serializers.RelatedField):
    def to_representation(self, value):
        print('**********')
        print(value)

    def to_internal_value(self, data):
        print("&&&&&&&&&&&&")
        print(data)
        return data

class BookSerializer(serializers.ModelSerializer):
    user = TrackListingField(queryset=Employee.objects.all())
    class Meta:
        model = Book
        fields = ('name', 'user')




